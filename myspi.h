#ifndef MYSPI_H_
#define MYSPI_H_

#include "main.h"

void spi_init(void);
uint8_t writeData(uint8_t data);

#endif
