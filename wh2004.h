#ifndef _WH2004_H_
#define _WH2004_H_
#include "main.h"

// #define 	LCD_CONTR_PORT		GPIOA		
// #define 	LCD_CONTR_PIN		GPIO_Pin_6
// Set RS port
#define MT_WH1602_RS_PORT     (GPIOB)
// Set RS pin
#define MT_WH1602_RS_PIN			(GPIO_Pin_10)
 
// Set RW port
#define MT_WH1602_RW_PORT			(GPIOB)	
// Set RW pin
#define MT_WH1602_RW_PIN			(GPIO_Pin_12)
 
// Set E port
#define MT_WH1602_E_PORT			(GPIOB)	
// Set E pin
#define MT_WH1602_E_PIN	  		(GPIO_Pin_11)


// Set DB7 port
#define MT_WH1602_DB7_PORT		(GPIOB)	
// Set DB7 pin
#define MT_WH1602_DB7_PIN			(GPIO_Pin_2)
 
// Set DB6 port
#define MT_WH1602_DB6_PORT		(GPIOB)	
// Set DB6 pin
#define MT_WH1602_DB6_PIN     (GPIO_Pin_14)
 
// Set DB5 port
#define MT_WH1602_DB5_PORT		(GPIOB)
// Set DB5 pin
#define MT_WH1602_DB5_PIN	    (GPIO_Pin_13)
 
// Set DB4 port
#define MT_WH1602_DB4_PORT		(GPIOB)
// Set DB4 pin
#define MT_WH1602_DB4_PIN		(GPIO_Pin_15)

#define 	LCD_RS_PORT			GPIOB
#define 	LCD_RW_PORT			GPIOB
#define 	LCD_E_PORT			GPIOB

#define 	LCD_D7_PORT			GPIOB
#define 	LCD_D6_PORT			GPIOB
#define 	LCD_D5_PORT			GPIOB
#define 	LCD_D4_PORT			GPIOB

#define 	LCD_RS_PIN			GPIO_Pin_10
#define 	LCD_RW_PIN			GPIO_Pin_12
#define 	LCD_E_PIN				GPIO_Pin_11
#define 	LCD_D4_PIN			GPIO_Pin_15
#define 	LCD_D5_PIN			GPIO_Pin_13
#define 	LCD_D6_PIN			GPIO_Pin_14
#define 	LCD_D7_PIN			GPIO_Pin_2

typedef struct 
{
	uint8_t x_pos;
	uint8_t	y_pos;
	uint8_t blink;
	char	message[16];
} LCDmessage;


void wh_init(void);
void wh_set_cursor(uint8_t line, uint8_t pos);
void wh_write_str(char* string);
void wh_write_chr(char c);
void lcd_clear(void);
void lcd_clear_line(uint8_t line);
//void delay_ms(uint32_t ms);
#endif // _WH2004_H_
