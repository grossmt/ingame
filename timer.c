/*****************************************************************************
 *   timer.c:  Timer C 
 *
 *
******************************************************************************/
    
#include "timer.h"


volatile 	u32 			gametimer=0;
volatile	LTimer		ltimers[MAX_LTIMERS_COUNT];
volatile	uint32_t	flagdel1ms=0;
volatile 	uint32_t  beep_delay=0;
extern int who_pressed;

/*******************************************************************************
* Function Name  : Timer_Configuration
* Description    : Configures the Timers.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void Timer_Configuration(void)
{
	NVIC_InitTypeDef NVIC_InitStructure;
	
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM3, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM4, ENABLE);
  
	TIM2->PSC = SystemCoreClock*0.00001;//40; //10uS  SystemCoreClock*0.00001, AHB prescaler=2
  TIM2->ARR = 1000; //10mc
  TIM2->DIER |= TIM_DIER_UIE; 
  TIM2->CR1 |= TIM_CR1_CEN;
  
  TIM3->PSC = SystemCoreClock*0.00001;//40; //10uS  SystemCoreClock*0.00001
  TIM3->ARR = 100; //1mc
  TIM3->DIER |= TIM_DIER_UIE; 
  TIM3->CR1 |= TIM_CR1_CEN;
	
	
	TIM4->PSC = SystemCoreClock*0.000001; //1uS  
	TIM4->CR1 |= TIM_CR1_CEN | TIM_CR1_OPM |TIM_CR1_DIR;
	
	NVIC_InitStructure.NVIC_IRQChannel = TIM2_IRQn ;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x07;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);

	NVIC_InitStructure.NVIC_IRQChannel = TIM3_IRQn ;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);	
}
/*******************************************************************************
* Function Name  : SYSTick_Configuration
* Description    : Configures systick mode and reload.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void SysTick_Configuration(void)
{
    /* SysTick interrupt each 250 ms with SysTick Clock equal to (HCLK/8) */
    if (SysTick_Config((SystemCoreClock / 8) / 4))
    {
        /* Capture error */
        while (1);
    }

    /* Select AHB clock(HCLK) divided by 8 as SysTick clock source */
    SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK_Div8);

    /* Set SysTick Preemption Priority to 1 */
    NVIC_SetPriority(SysTick_IRQn, 0x04);
}
/***********************************************************************************/
uint32_t GetGameTimer(void)
{
	return gametimer;
}
/***********************************************************************************/
void SetGameTimer(u32 value)
{
	gametimer=value;
}
/***********************************************************************************/
#define ENABLE_SCAN
void TIM3_IRQHandler(void) //every 1 ms
{
	//static char time10=0;
	static int flash=1000;
	static int counter_second=0;
	
	TIM3->SR &= ~TIM_SR_UIF;
	if (flagdel1ms!=0) flagdel1ms--;

	if (flash) 
		flash--; 
	else	
		flash=1000;

	if (who_pressed >=0 || counter_second >=3000)
		counter_second = 0;
	
	//reset all LEDs before key testing
#ifdef ENABLE_SCAN	
	PIN_OFF(LED1);
	PIN_OFF(LED2);
	PIN_OFF(LED3);
	PIN_OFF(LED4);
	PIN_OFF(LED5);
	PIN_OFF(LED6);
	PIN_OFF(LED7);
	PIN_OFF(LED8);
//UPDATE PLAYERS STATUS EVERY 1ms	

//player 0 	#1
	{
		
		player[0].keystate=PIN_SIGNAL(SW7); 	//this attribute independently from enabled ???? 
			
		switch (player[0].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED7);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED7);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[0].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED7); //middle flash
			}
			break;
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED7); //middle flash				
			}
			break;			
		}
		
		if (player[0].enabled)
		{
//			if (player[0].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[0].keystate)
				{
//					player[0].keypressed = 1;
					player[0].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 0;
					
				}
			}
		}
		else
		{
			player[0].keytimestamp=0;
		}
	}
	
//player 1 	#2
	{
		
		player[1].keystate=PIN_SIGNAL(SW5); 	//this attribute independently from enabled ???? 
			
		switch (player[1].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED5);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED5);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[1].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED5); //middle flash
			}
			break;
			
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED5); //middle flash				
			}
			break;
		}
		
		if (player[1].enabled)
		{
			//if (player[1].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[1].keystate)
				{
					//player[1].keypressed = 1;
					player[1].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 1;
					
				}
			}
		}
		else
		{
			player[1].keytimestamp=0;
		}
	}
	
//player 2 	#3
	{
		
		player[2].keystate=PIN_SIGNAL(SW3); 	//this attribute independently from enabled ???? 
			
		switch (player[2].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED3);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED3);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[2].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED3); //middle flash
			}
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED3); //middle flash				
			}
			break;			

		}
		
		if (player[2].enabled)
		{
//			if (player[2].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[2].keystate)
				{
//					player[2].keypressed = 1;
					player[2].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 2;
					
				}
			}
		}
		else
		{
			player[2].keytimestamp=0;
		}
	}	
//player 3	#4
	{
		
		player[3].keystate=PIN_SIGNAL(SW1); 	//this attribute independently from enabled ???? 
			
		switch (player[3].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED1);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED1);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[3].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED1); //middle flash
			}
			break;
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED1); //middle flash				
			}
			break;			
		}
		
		if (player[3].enabled)
		{
//			if (player[3].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[3].keystate)
				{
//					player[3].keypressed = 1;
					player[3].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 3;
					
				}
			}
		}
		else
		{
			player[3].keytimestamp=0;
		}
	}	
		
//player 4 	#5
	{
		
		player[4].keystate=PIN_SIGNAL(SW2); 	//this attribute independently from enabled ???? 
			
		switch (player[4].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED2);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED2);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[4].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED2); //middle flash
			}
			break;
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED2); //middle flash				
			}
			break;			
		}
		
		if (player[4].enabled)
		{
//			if (player[4].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[4].keystate)
				{
//					player[4].keypressed = 1;
					player[4].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 4;
					
				}
			}
		}
		else
		{
			player[4].keytimestamp=0;
		}
	}	
		
//player 5 	#6
	{
		
		player[5].keystate=PIN_SIGNAL(SW4); 	//this attribute independently from enabled ???? 
			
		switch (player[5].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED4);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED4);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[5].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED4); //middle flash
			}
			break;
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED4); //middle flash				
			}
			break;			
		}
		
		if (player[5].enabled)
		{
//			if (player[5].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[5].keystate)
				{
//					player[5].keypressed = 1;
					player[5].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 5;
					
				}
			}
		}
		else
		{
			player[5].keytimestamp=0;
		}
	}	
		
//player 6 	#7
	{
		
		player[6].keystate=PIN_SIGNAL(SW6); 	//this attribute independently from enabled ???? 
			
		switch (player[6].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED6);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED6);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[6].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED6); //middle flash
			}
			break;
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED6); //middle flash				
			}
			break;			
		}
		
		if (player[6].enabled)
		{
//			if (player[6].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[6].keystate)
				{
	//				player[6].keypressed = 1;
					player[6].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 6;
					
				}
			}
		}
		else
		{
			player[6].keytimestamp=0;
		}
	}	
		
	
//player 7	#8
	{
		
		player[7].keystate=PIN_SIGNAL(SW8); 	//this attribute independently from enabled ???? 
			
		switch (player[7].ledstate)
		{
			case NOTHING:
			{
				
				PIN_OFF(LED8);
			}
			break;
			case PERMANENT:
			{
				PIN_ON(LED8);
			}
			break;
			case BLINKING3S:
			{

				counter_second ++;
				
				if (counter_second >=3000)
					player[7].ledstate = NOTHING;
				
				if (flash<500)
					PIN_ON(LED8); //middle flash
			}
			break;
			case BLINKING:
			{
				if (flash<500)
					PIN_ON(LED8); //middle flash				
			}
			break;			
		}
		
		if (player[7].enabled)
		{
//			if (player[7].keypressed==0)  //we can change keypressed only if previosly it was in 0
			{
				if (player[7].keystate)
				{
//					player[7].keypressed = 1;
					player[7].keytimestamp=gametimer;
					
					if (who_pressed == -1)
						who_pressed = 7;
					
				}
			}
		}
		else
		{
			player[7].keytimestamp=0;
		}
	}	
		
	
	
	
	
#endif
}
/***********************************************************************************/
void TIM2_IRQHandler(void) //every 10 ms
{
		static char beep_off = 0;

		TIM2->SR &= ~TIM_SR_UIF; //
		ProcessLtimers();
		DecRxTimeout();
	
		gametimer++;
		
		if (beep_delay == 0)
		{
			if (beep_off == 1)
			{
				beep_off = 0;
				GenerateSound(DISABLE);
			}
		}
		else
		{
			beep_delay--;
			beep_off = 1;
		}
			
 }

/***********************************************************************************/
void SetBeepDelay(uint32_t timeout)
{
	beep_delay = timeout;
}	
/***********************************************************************************/
void ProcessLtimers(void)
{
u08 i;
for (i=0;i<MAX_LTIMERS_COUNT;i++)
	{
	if ((ltimers[i].lock==0)&&(ltimers[i].timer)!=0) ltimers[i].timer--; 
	}
}
/***********************************************************************************/
void SetLTimer(uint8_t ltmr,uint32_t time)
{
ltimers[ltmr].lock=1;
ltimers[ltmr].timer=time;
ltimers[ltmr].lock=0;
}
/***********************************************************************************/
int CheckLTimer(uint8_t ltmr)
{
if (ltimers[ltmr].timer==0) return TRUE;
return FALSE; 
}
/***********************************************************************************/
void delay(uint32_t del)
{
 SetLTimer(TIMER_DEL10MS,del);
 while (ltimers[TIMER_DEL10MS].timer!=0);
}
/***********************************************************************************/
void delayms(uint32_t del)
{
	flagdel1ms=del;	
	while (flagdel1ms!=0)
	{
		
	}
}
/***********************************************************************************/
void setdel(uint32_t del)
{
	flagdel1ms=del;	
}
/***********************************************************************************/
uint32_t getdel(void)
{
	return flagdel1ms;
}
void delay_us_T4 (uint16_t delay)
{
	TIM4->PSC = SystemCoreClock*0.0001; //  
  TIM4->CNT = delay;
  //TIM4->DIER |= TIM_DIER_UIE; 
  TIM4->CR1 |= TIM_CR1_CEN | TIM_CR1_OPM |TIM_CR1_DIR;
	while ((TIM4->CR1&TIM_CR1_CEN)!=0);
}
/************************************************************************************
**                                End Of File
************************************************************************************/
