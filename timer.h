/*****************************************************************************
 *   rit_timer.h:  Header file for NXP LPC17xx Family Microprocessors
 *
 *
******************************************************************************/
#ifndef __RIT_TIMER_H 
#define __RIT_TIMER_H


#include "main.h"

typedef struct
{
u08	lock;
u32 timer;
} LTimer;


//Ltimers clock with 10ms resolution
//count of ltimers
#define MAX_LTIMERS_COUNT		6
//name of ltimers
#define TIMER_WAIT					0 //general purpose wait event
#define TIMER_DEL10MS				1 //for 10ms resolution delay
#define TIMER_OWI_WAIT			2 //general purpose wait event
#define TIMER_KEY1_WAIT			3 //general purpose wait event
#define TIMER_KEY2_WAIT			4 //general purpose wait event
#define TIMER_KEY3_WAIT			5 //general purpose wait event


void Timer_Configuration(void);
void SysTick_Configuration(void);			   
u32 	GetSysCounter(void);
void 	SysTickProcess (void);
void 	ProcessLtimers(void);
void 	SetLTimer(u08 ltmr,u32 time);
int 	CheckLTimer(u08 ltmr);
void 	delay(uint32_t del);
void 	PrintSysTime(void);
u32 	GetSystick(void);
void	delayms(uint32_t del);
void setdel(uint32_t del);
uint32_t getdel(void);
void delay_us_T4 (uint16_t delay);
uint32_t GetGameTimer(void);
void SetGameTimer(u32 value);
void SetBeepDelay(uint32_t timeout);
#endif /* end __RIT_TIMER_H */
/*****************************************************************************
**                            End Of File
******************************************************************************/
