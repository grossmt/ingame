/******************** (C) COPYRIGHT 2011 STMicroelectronics ********************
* File Name          : hw_config.c
* Author             : MCD Application Team
* Version            : V3.3.0
* Date               : 21-March-2011
* Description        : Hardware Configuration & Setup
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/


#include "hw_config.h"
#define VECT_TAB_OFFSET  0x0
/*******************************************************************************
* Function Name  : USART_Configuration
* Description    : Configures the start different USART ports.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void USART_Configuration(void)
{
  USART_InitTypeDef USART_InitStructure;  
  NVIC_InitTypeDef NVIC_InitStructure;
  
  //USART1
#ifdef USART1_EN	
  USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
  USART_Cmd(USART1, DISABLE);    
  USART_DeInit(USART1); 
  RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE); // UART1  
  
	USART_InitStructure.USART_BaudRate = USART1_BAUDRATE;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1;
  USART_InitStructure.USART_Parity = USART_Parity_No;
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;               
  USART_Init(USART1, &USART_InitStructure);    
  
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);    
  NVIC_InitStructure.NVIC_IRQChannel = USART1_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);  
  
	USART_Cmd(USART1, ENABLE);    
  USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);   
#endif
  
  //USART2 
#ifdef USART2_EN	
  USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
  USART_Cmd(USART2, DISABLE); 
  USART_DeInit(USART2);  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART2, ENABLE); 
  
  USART_InitStructure.USART_BaudRate = USART2_BAUDRATE;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1; //USART_StopBits_2;                                        ///!!!
  USART_InitStructure.USART_Parity = USART_Parity_No; //USART_Parity_Even;                                         ///!!!
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;               
  USART_Init(USART2, &USART_InitStructure);    

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);    
  NVIC_InitStructure.NVIC_IRQChannel = USART2_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);  

  USART_Cmd(USART2, ENABLE);    
  USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);  
#endif	
	//USART3 
#ifdef USART3_EN
  USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
  USART_Cmd(USART3, DISABLE); 
  USART_DeInit(USART3);  
  RCC_APB1PeriphClockCmd(RCC_APB1Periph_USART3, ENABLE); 
  
  USART_InitStructure.USART_BaudRate = USART3_BAUDRATE;
  USART_InitStructure.USART_WordLength = USART_WordLength_8b;
  USART_InitStructure.USART_StopBits = USART_StopBits_1; //USART_StopBits_2;                                        ///!!!
  USART_InitStructure.USART_Parity = USART_Parity_No; //USART_Parity_Even;                                         ///!!!
  USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
  USART_InitStructure.USART_Mode = USART_Mode_Rx | USART_Mode_Tx;               
  USART_Init(USART3, &USART_InitStructure);    

  NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);    
  NVIC_InitStructure.NVIC_IRQChannel = USART3_IRQn;
  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 2;
  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
  NVIC_Init(&NVIC_InitStructure);  

  USART_Cmd(USART3, ENABLE);    
  USART_ITConfig(USART3, USART_IT_RXNE, ENABLE); 
#endif	
}
/*******************************************************************************
* Function Name  : GPIO_Configuration
* Description    : Configures the different GPIO ports.
* Input          : None
* Output         : None
* Return         : None
*******************************************************************************/
void GPIO_Configuration(void)
{
  GPIO_InitTypeDef GPIO_InitStructure;
	
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOA , ENABLE);
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOB , ENABLE);
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOC , ENABLE);
  RCC_APB2PeriphClockCmd(	 RCC_APB2Periph_SYSCFG, ENABLE);

	PIN_CONFIGURATION(LED1);	
	PIN_CONFIGURATION(LED2);
	PIN_CONFIGURATION(LED3);
	PIN_CONFIGURATION(LED4);
	PIN_CONFIGURATION(LED5);
	PIN_CONFIGURATION(LED6);
	PIN_CONFIGURATION(LED7);
	PIN_CONFIGURATION(LED8);
	
	PIN_CONFIGURATION(KEY1P);
	PIN_CONFIGURATION(KEY2P);
	PIN_CONFIGURATION(KEY3P);
	PIN_CONFIGURATION(KEY4P);
	
	PIN_CONFIGURATION(SW1);
	PIN_CONFIGURATION(SW2);
	PIN_CONFIGURATION(SW3);
	PIN_CONFIGURATION(SW4);
	PIN_CONFIGURATION(SW5);
	PIN_CONFIGURATION(SW6);
	PIN_CONFIGURATION(SW7);
	PIN_CONFIGURATION(SW8);
	
//	PIN_CONFIGURATION(BLIGHT);
	
	
	
#ifdef USART1_EN 
	// --  UART1 --------------------------------------------------------
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource9, GPIO_AF_USART1); 
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource10, GPIO_AF_USART1); 
  
  // TX UART.
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_9;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure); 
  
  // RX UART. 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_Init(GPIOA, &GPIO_InitStructure);     
  // ---------------------------------------------------------------------------
#endif		
#ifdef USART2_EN  
  // --  UART2 --------------------------------------------------------
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource2, GPIO_AF_USART2); 
  GPIO_PinAFConfig(GPIOA, GPIO_PinSource3, GPIO_AF_USART2); 
  
  // TX UART.
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(GPIOA, &GPIO_InitStructure); 
  
  // RX UART. 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_3;
  GPIO_Init(GPIOA, &GPIO_InitStructure);     
  // -----------------------------------------------------------------
#endif		
#ifdef USART3_EN
  // --  UART3 -------------------------------------------------------
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource10, GPIO_AF_USART3); 
  GPIO_PinAFConfig(GPIOB, GPIO_PinSource11, GPIO_AF_USART3); 
  
  // TX UART.
  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_UP;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_10;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure); 
  
  // RX UART. 
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_11;
  GPIO_Init(GPIOB, &GPIO_InitStructure);     
  // ---------------------------------------------------------------------------
#endif	

/*
// Configure ADC (BAT and motor current measure) pin as Analogue 	
//  GPIO_InitStructure.GPIO_Pin = PIN_BATV ;                               
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AN;
//  GPIO_Init( GPIOA, &GPIO_InitStructure);
// Configure SWD pins //	
//  GPIO_PinAFConfig(GPIOA, GPIO_PinSource13, GPIO_AF_SWJ); 
//  GPIO_PinAFConfig(GPIOA, GPIO_PinSource14, GPIO_AF_SWJ); 
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
//  GPIO_InitStructure.GPIO_Pin = 	GPIO_Pin_13|GPIO_Pin_14;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_40MHz;
//  GPIO_Init(GPIOA, &GPIO_InitStructure);	
// Configure WakeUp 	
//	GPIO_PinAFConfig(GPIOA, GPIO_PinSource0, GPIO_AF_WKUP); 
//  GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
//  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_NOPULL;
//  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_AF;
//  GPIO_InitStructure.GPIO_Pin = 	GPIO_Pin_0;
//  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
//  GPIO_Init(GPIOA, &GPIO_InitStructure);*/
}
/*******************************************************************************
*******************************************************************************/
void EXTI_init(void)
{
//	EXTI_InitTypeDef EXTI_InitStructure;
//  NVIC_InitTypeDef NVIC_InitStructure;
	RCC_APB2PeriphClockCmd(		RCC_APB2Periph_SYSCFG, ENABLE);
//	/* Select P1 pin as input source for EXTI0 Line */
//	{
//  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA,EXTI_PinSource0);
//  /* Configure EXT1 Line 0 in interrupt mode trigged on Rising edge */
//  EXTI_InitStructure.EXTI_Line = EXTI_Line0 ;  // PA0 for TILT sensor
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//  /* Enable and set EXTI0 Interrupt to the lowest priority */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI0_IRQn ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
//	}

  /* Select  pin as input source for EXTI1 Line */
	{
//  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource1);
//  /* Configure EXT1 Line 0 in interrupt mode trigged on Rising edge */
//  EXTI_InitStructure.EXTI_Line = EXTI_Line1 ;  // PB1 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//  /* Enable and set EXTI1 Interrupt to the lowest priority */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI1_IRQn ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
	}
	
	/* Select  pin as input source for EXTI3 Line */
	{
//  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA,EXTI_PinSource3);
//  /* Configure EXT1 Line 0 in interrupt mode trigged on Rising edge */
//  EXTI_InitStructure.EXTI_Line = EXTI_Line3 ;  // PA3 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//  /* Enable and set EXTI1 Interrupt to the lowest priority */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI3_IRQn ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
	}

  /* Select pins as input source for EXTI9-5 Lines */
	{
//  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA,EXTI_PinSource6);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line6 ;  // PA6 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//  SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA,EXTI_PinSource7);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line7 ;  //  PA7 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOA,EXTI_PinSource8);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line8 ;  //  PA8 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource9);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line9 ;  //  PB9 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);		
//  /* Enable and set EXTI9_5 Interrupt to the lowest priority */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI9_5_IRQn ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x0F;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
	}

  /* Select pins as input source for EXTI10-15 Lines */
	{
//		SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource12);
////  /* Configure EXT1 Line 0 in interrupt mode trigged on Rising edge */
//  EXTI_InitStructure.EXTI_Line = EXTI_Line12 ;  // PB12 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//		 SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource13);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line13 ;  // PB13 for 
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);	
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource14);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line14 ;  // PB14 for EXT SW
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising_Falling;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);
//	SYSCFG_EXTILineConfig(EXTI_PortSourceGPIOB,EXTI_PinSource15);
//  EXTI_InitStructure.EXTI_Line = EXTI_Line15 ;  // PB15 for VUSB
//  EXTI_InitStructure.EXTI_Mode = EXTI_Mode_Interrupt;
//  EXTI_InitStructure.EXTI_Trigger = EXTI_Trigger_Rising;  
//  EXTI_InitStructure.EXTI_LineCmd = ENABLE;
//  EXTI_Init(&EXTI_InitStructure);		
  /* Enable and set EXTI1 Interrupt to the lowest priority */
//  NVIC_InitStructure.NVIC_IRQChannel = EXTI15_10_IRQn ;
//  NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority = 0x03;
//  NVIC_InitStructure.NVIC_IRQChannelSubPriority = 0x03;
//  NVIC_InitStructure.NVIC_IRQChannelCmd = ENABLE;
//  NVIC_Init(&NVIC_InitStructure);
	}	
}


/*******************************************************************************
* Function Name  : Set_HWSystem
* Description    : Configures Main system clocks & power.
* Input          : None.
* Return         : None.
*******************************************************************************/
void Set_HWSystem(void)
{
//	SystemCoreClockUpdate();	
//	RCC_AHBPeriphClockCmd (   RCC_AHBPeriph_GPIOA , ENABLE);
//	RCC_AHBPeriphClockCmd (   RCC_AHBPeriph_GPIOB , ENABLE);
//  RCC_APB2PeriphClockCmd(	 RCC_APB2Periph_SYSCFG, ENABLE);
  GPIO_Configuration();
	//EXTI_init();
  USART_Configuration();
	Timer_Configuration();
	//#ifdef USE_WH1602
	wh_lcd_init();
	//#endif
//	#ifdef USE_LCD320240
//	spi_init();	
//	PIN_CONFIGURATION(RS_RESD);
//	PIN_CONFIGURATION(EN_DC);
//	PIN_CONFIGURATION(RW_CS);	
//	TFT_init();
//	#endif
	//DAC_init();
	//PWR_VoltageScalingConfig(PWR_VoltageScaling_Range2);
}
/*-------------------------END-------------------------------*/




