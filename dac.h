#ifndef DAC_H_
#define DAC_H_

#include "main.h"

//#define LEVELNEW

void Sinus_calculate(uint16_t level);
void DAC_init(char level);
void DAC_init_var(char level);
void DAC_init_Triangle(void);
void SetSound(uint32_t frequency);
void GenerateSound(FunctionalState State);
void BEEP(uint32_t frequency, uint32_t timeout, char level);

	
	
#endif	
