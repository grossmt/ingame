#ifndef MYTFT_H_
#define MYTFT_H_

#include "main.h"

/*-----------------------------------------------------------*/
#define TFT_PORT        GPIOB
#define TFT_SPI         SPI2
#define _delay_us				delay_us_T4

/*-----------------------------------------------------------*/
//#define PIN_LED         GPIO_Pin_1
#define PIN_RST         GPIO_Pin_10
#define PIN_DC          GPIO_Pin_11
#define PIN_CS          GPIO_Pin_12
#define PIN_SCK         GPIO_Pin_13
#define PIN_MISO        GPIO_Pin_14
#define PIN_MOSI        GPIO_Pin_15

/*-----------------------------------------------------------*/
// Colors
#define RED             0xf800
#define GREEN           0x07e0
#define BLUE            0x001f
#define BLACK           0x0000
#define YELLOW          0xffe0
#define WHITE           0xffff
#define CYAN            0x07ff
#define BRIGHT_RED      0xf810
#define GRAY1           0x8410
#define GRAY2           0x4208

/*-----------------------------------------------------------*/
#define TFT_CS_LOW              PIN_OFF(RW_CS);//GPIO_ResetBits(TFT_PORT, PIN_CS);
#define TFT_CS_HIGH             PIN_ON(RW_CS);//GPIO_SetBits(TFT_PORT, PIN_CS);
#define TFT_DC_LOW              PIN_OFF(EN_DC);//GPIO_ResetBits(TFT_PORT, PIN_DC);
#define TFT_DC_HIGH             PIN_ON(EN_DC);//GPIO_SetBits(TFT_PORT, PIN_DC);
#define TFT_RST_LOW             PIN_OFF(RS_RESD);//GPIO_ResetBits(TFT_PORT, PIN_RST);
#define TFT_RST_HIGH    				PIN_ON(RS_RESD);//GPIO_SetBits(TFT_PORT, PIN_RST);
/*-----------------------------------------------------------*/

void TFT_sendByte(uint8_t data);
void TFT_sendCMD(int index);
void TFT_sendDATA(int data);
void TFT_sendWord(int data);
int TFT_Read_Register(int Addr, int xParameter);
int TFT_readID(void);
void TFT_init(void);
void TFT_led(int state);
void TFT_setCol(int StartCol, int EndCol);
void TFT_setPage(int StartPage, int EndPage);
void TFT_setXY(int poX, int poY);
void TFT_setPixel(int poX, int poY, int color);
void fillScreen(void);

#endif
