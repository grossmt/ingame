#include "tft320240.h"

/*-----------------------------------------------------------*/



/*-----------------------------------------------------------*/
void TFT_sendWord(int data)
{
        while(SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_BSY) == SET)
				{
				}
    TFT_DC_HIGH;

    TFT_sendByte(data >> 8);
    TFT_sendByte(data & 0x00ff);
}
/*-----------------------------------------------------------*/
void TFT_sendByte(uint8_t data)
{
   while(SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_TXE)==RESET)
	 {
	 }
   SPI_I2S_SendData(TFT_SPI, data);
}

/*-----------------------------------------------------------*/
void TFT_sendCMD(int index)
{
    while(SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_BSY) == SET)
		{
		}
    TFT_CS_HIGH;
    TFT_DC_LOW;
    TFT_CS_LOW;
    TFT_sendByte(index);
          _delay_us(50);
}

/*-----------------------------------------------------------*/
void TFT_sendDATA(int data)
{
        while(SPI_I2S_GetFlagStatus(TFT_SPI, SPI_I2S_FLAG_BSY) == SET)
				{
				}
    TFT_DC_HIGH;
    TFT_sendByte(data);
}



/*-----------------------------------------------------------*/
int TFT_Read_Register(int Addr, int xParameter)
{
        int data = 0;

    TFT_sendCMD(0xD9);                                                    // ext command
    TFT_sendByte(0x10+xParameter);                         // 0x11 is the first Parameter
    TFT_DC_LOW;
    TFT_CS_LOW;
    TFT_sendByte(Addr);
    TFT_DC_HIGH;
    data = SPI_I2S_ReceiveData(TFT_SPI);
    TFT_CS_HIGH;

    return data;
}

/*-----------------------------------------------------------*/
int TFT_readID(void)
{
        int i = 0;
        int byte;
        int data[3] = {0x00, 0x00, 0x00};
        int ID[3] = {0x00, 0x93, 0x41};
        int ToF = 1;

        for(i=0; i<3; i++)
    {
                byte = TFT_Read_Register(0xD3, i+1);
        data[i] = byte;

        if(data[i] != ID[i])
        {
            ToF = 0;
        }
    }

    return ToF;
}

/*-----------------------------------------------------------*/
void TFT_init(void)
{

				spi_init();
				
				PIN_CONFIGURATION(RS_RESD);
				PIN_CONFIGURATION(EN_DC);
				PIN_CONFIGURATION(RW_CS);
	
        TFT_RST_LOW;
        TFT_RST_HIGH;

				TFT_sendCMD(0xCB);
        TFT_sendDATA(0x39);
        TFT_sendDATA(0x2C);
        TFT_sendDATA(0x00);
        TFT_sendDATA(0x34);
        TFT_sendDATA(0x02);

        TFT_sendCMD(0xCF);
        TFT_sendDATA(0x00);
        TFT_sendDATA(0xC1);
        TFT_sendDATA(0x30);

        TFT_sendCMD(0xE8);
        TFT_sendDATA(0x85);
        TFT_sendDATA(0x00);
        TFT_sendDATA(0x78);

        TFT_sendCMD(0xEA);
        TFT_sendDATA(0x00);
        TFT_sendDATA(0x00);

        TFT_sendCMD(0xED);
        TFT_sendDATA(0x64);
        TFT_sendDATA(0x03);
        TFT_sendDATA(0x12);
        TFT_sendDATA(0x81);

        TFT_sendCMD(0xF7);
        TFT_sendDATA(0x20);

        TFT_sendCMD(0xC0);      //Power control
        TFT_sendDATA(0x23);     //VRH[5:0]

        TFT_sendCMD(0xC1);      //Power control
        TFT_sendDATA(0x10);     //SAP[2:0];BT[3:0]

        TFT_sendCMD(0xC5);      //VCM control
        TFT_sendDATA(0x3e);     //Contrast
        TFT_sendDATA(0x28);

        TFT_sendCMD(0xC7);      //VCM control2
        TFT_sendDATA(0x86);      //--

        TFT_sendCMD(0x36);      // Memory Access Control
        TFT_sendDATA(0x48);     //C8       //48 68?��? �??�//28 E8 ?�???�??�

        TFT_sendCMD(0x3A); 
        TFT_sendDATA(0x55); 

        TFT_sendCMD(0xB1); 
        TFT_sendDATA(0x00); 
        TFT_sendDATA(0x18); 

        TFT_sendCMD(0xB6);      // Display Function Control
        TFT_sendDATA(0x08); 
        TFT_sendDATA(0x82); 
        TFT_sendDATA(0x27); 

        TFT_sendCMD(0xF2);      // 3Gamma Function Disable
        TFT_sendDATA(0x00); 

        TFT_sendCMD(0x26);      //Gamma curve selected
        TFT_sendDATA(0x01);  

        TFT_sendCMD(0xE0);      //Set Gamma
        TFT_sendDATA(0x0F); 
        TFT_sendDATA(0x31); 
        TFT_sendDATA(0x2B); 
        TFT_sendDATA(0x0C); 
        TFT_sendDATA(0x0E); 
        TFT_sendDATA(0x08); 
        TFT_sendDATA(0x4E); 
        TFT_sendDATA(0xF1); 
        TFT_sendDATA(0x37); 
        TFT_sendDATA(0x07); 
        TFT_sendDATA(0x10); 
        TFT_sendDATA(0x03); 
        TFT_sendDATA(0x0E); 
        TFT_sendDATA(0x09); 
        TFT_sendDATA(0x00); 

        TFT_sendCMD(0xE1);      //Set Gamma
        TFT_sendDATA(0x00); 
        TFT_sendDATA(0x0E); 
        TFT_sendDATA(0x14); 
        TFT_sendDATA(0x03); 
        TFT_sendDATA(0x11); 
        TFT_sendDATA(0x07); 
        TFT_sendDATA(0x31); 
        TFT_sendDATA(0xC1); 
        TFT_sendDATA(0x48); 
        TFT_sendDATA(0x08); 
        TFT_sendDATA(0x0F); 
        TFT_sendDATA(0x0C); 
        TFT_sendDATA(0x31); 
        TFT_sendDATA(0x36); 
        TFT_sendDATA(0x0F); 
        TFT_sendCMD(0x11);      //Exit Sleep
        TFT_sendCMD(0x29);    //Display on
        TFT_sendCMD(0x2c); 
}

/*-----------------------------------------------------------*/
void TFT_led(int state)
{
//        if (state)
//                GPIO_WriteBit(TFT_PORT, PIN_LED, Bit_SET);
//        else
//                GPIO_WriteBit(TFT_PORT, PIN_LED, Bit_RESET);
}

/*-----------------------------------------------------------*/
void TFT_setCol(int StartCol, int EndCol)
{
        TFT_sendCMD(0x2A);                                                     // Column Command address
        TFT_sendWord(StartCol);
        TFT_sendWord(EndCol);
}

/*-----------------------------------------------------------*/
void TFT_setPage(int StartPage, int EndPage)
{
        TFT_sendCMD(0x2B);                                                  // Column Command address
        TFT_sendWord(StartPage);
        TFT_sendWord(EndPage);
}

/*-----------------------------------------------------------*/
void TFT_setXY(int poX, int poY)
{
        TFT_setCol(poX, poX);
        TFT_setPage(poY, poY);
        TFT_sendCMD(0x2c);
}

/*-----------------------------------------------------------*/
void TFT_setPixel(int poX, int poY, int color)
{
        TFT_setXY(poX, poY);
        TFT_sendWord(color);
}


/*-----------------------------------------------------------*/
void fillScreen(void)
{
    TFT_setCol(0, 239);
    TFT_setPage(0, 319);
    TFT_sendCMD(0x2c);                                                  /* start to write to display ra */
                                                                        /* m                            */

    TFT_DC_HIGH;
    TFT_CS_LOW;
    for(i=0; i<38400; i++)
    {
       TFT_sendByte(0);
        TFT_sendByte(0);
       TFT_sendByte(0);
        TFT_sendByte(0);
    }
    TFT_CS_HIGH;
}
/*-----------------------------------------------------------*/

/*
//example of using
        TFT_setPixel(10, 10, BLUE);

        fillScreen();
        
        for(i=0; i<100; i++) {
                TFT_setPixel(i, 50, RED);
                TFT_setPixel(i, 51, RED);

                TFT_setPixel(50, i, GREEN);
                TFT_setPixel(51, i, GREEN);
        }

        for(i=50; i<200; i++) {
                for(j=50; j<200; j++) {
                        TFT_setPixel(i, j, BLUE);
                }
        }
*/
