#ifndef LCD_H_
#define LCD_H_

	#include "main.h"


#define LINE0		0
#define LINE1		1
#define LINE2		2
#define LINE3		3

void wh_tetr_out(uint8_t tetr);
void wh_write_cmd(uint8_t cmd);
void wh_write_data(uint8_t data);
void wh_lcd_init(void); 
void wh_set_cursor(uint8_t line, uint8_t pos);
void wh_lcd_clear(void);
void wh_cursorEnable(uint8_t on) ;
void wh_write_str(char* string);
void wh_write_str_center_1602(char* string,uint8_t line);
void wh_write_chr(char c);
void lcdProgressBar(uint8_t progress, uint8_t line);
int DefineChar(int charnum, char* values);
#endif /*LCD_H_*/
