#ifndef __MAIN_H
#define __MAIN_H

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>
#include "type.h"
#include "stm32l1xx.h"

#include "usart.h"
#include "type.h"
#include "macros.h" 
#include "hw_config.h"
#include "timer.h"
#include "stm32l1xx_it.h"
#include "lcdmy.h"
#include "dac.h"
//#include "swodebug.h"
//#include "myspi.h"
//#include "tft_lcd.h"

#define USE_WH1602
//#define USE_LCD320240

//FreeRTOS
#define FREERTOS_USE

#include "FreeRTOS.h"
#include "FreeRTOSConfig.h"
#include "task.h"
#include "queue.h"


/*
all player's structures will be filled in Timer2IRQ every 1ms
*/
	typedef struct
	{
		char  enabled;					//enable disable reaction for this player
		char	keypressed;				// key was press or no.. Fix keypress event after enabled=1
		char	keystate;					//current state of key
		uint32_t	keytimestamp; //timestamp when key was pressed
		char	ledstate;					//LED state
	}
	Player;
	
	#define USART1_EN
//#define USART2_EN
//#define USART3_EN
	#define USART1_BAUDRATE 9600

	#define DEBUG_UART	1
	#define LCD_PRINT		7
	
	//types of game
	#define CHGK 0
	#define BR	 1	
	#define SI   2
	#define VS   3
	
	//states of game
	#define RESETED   	0
	#define COUNTDOWN		1
	#define FINISH			2		
	#define PAUSED			3
	#define ANSWER			4
	#define FALSTART		5
	#define RECOUNTDOWN	6
	#define WAITTORECOUNTDOWN 7
	#define PRINTANSWER 8
	
	//states of LED
	#define NOTHING 			0
	#define PERMANENT 		1
	#define BLINKING3S 		2
	#define BLINKING 			3
	
	//types of signal
	#define button_press 		1
	#define sound_start 		2
	#define sound_finish		3
	#define sound_falstart	4	
	#define sound_answer		5
	#define time_is_ending	6
	
//types of sound
#define BEEP_Start			0
#define BEEP_Stop				1
#define BEEP_Remind10		2
#define BEEP_Falstart		3
#define BEEP_Answer			4
#define BEEP_Key				5	

//extern RTC_TimeTypeDef cur_time;
extern Player player[8];

char FSMCHGK(void);
char FSMBR(void);
char FSMSI(void);
char FSMVS(void);
void wh_write_str_center(char* string,uint8_t line);

void ShowMode(uint8_t mode);
char ScanKey(void);
char keyPauseHandler(void);
char keyResetHandler(void);

#endif /* __MAIN_H */
