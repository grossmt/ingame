#include "wh2004.h"


#define WH_4BIT

#define E1   	GPIO_SetBits(LCD_E_PORT, LCD_E_PIN)
#define E0   	GPIO_ResetBits(LCD_E_PORT, LCD_E_PIN)
#define RW1  	GPIO_SetBits(LCD_RW_PORT, LCD_RW_PIN)
#define RW0  	GPIO_ResetBits(LCD_RW_PORT, LCD_RW_PIN)
#define RS1  	GPIO_SetBits(LCD_RS_PORT, LCD_RS_PIN)
#define RS0  	GPIO_ResetBits(LCD_RS_PORT, LCD_RS_PIN)

#define D7_0	GPIO_ResetBits(LCD_D7_PORT, LCD_D7_PIN)
#define D7_1	GPIO_SetBits(LCD_D7_PORT, LCD_D7_PIN)
#define D6_0	GPIO_ResetBits(LCD_D6_PORT, LCD_D6_PIN)
#define D6_1	GPIO_SetBits(LCD_D6_PORT, LCD_D6_PIN)
#define D5_0	GPIO_ResetBits(LCD_D5_PORT, LCD_D5_PIN)
#define D5_1	GPIO_SetBits(LCD_D5_PORT, LCD_D5_PIN)
#define D4_0	GPIO_ResetBits(LCD_D4_PORT, LCD_D4_PIN)
#define D4_1	GPIO_SetBits(LCD_D4_PORT, LCD_D4_PIN)


const unsigned char russian[]={ 0x41, 0xA0, 0x42, 0xA1, 0xE0, 0x45,
0xA3, 0xA4, 0xA5,0xA6, 0x4B, 0xA7, 0x4D, 0x48, 0x4F, 0xA8, 0x50,0x43,
0x54, 0xA9, 0xAA, 0x58, 0xE1, 0xAB, 0xAC, 0xE2, 0xAD,0xAE, 0x62,
0xAF, 0xB0, 0xB1, 0x61, 0xB2, 0xB3, 0xB4, 0xE3, 0x65, 0xB6, 0xB7,
0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0x6F, 0xBE, 0x70, 0x63,0xBF,
0x79, 0xE4, 0x78, 0xE5, 0xC0, 0xC1, 0xE6, 0xC2, 0xC3, 0xC4, 0xC5,
0xC6, 0xC7 };


#define delay_ms  delayms


// void delay_ms(uint32_t ms)
// {
//     static uint32_t i;
//  	for (i=0;i<(10000*ms);i++);
// }



void wh_tetr_out(uint8_t tetr)
{

	E1;
	if(tetr & 0x80) D7_1; else D7_0;
	if(tetr & 0x40) D6_1; else D6_0;
	if(tetr & 0x20) D5_1; else D5_0;
	if(tetr & 0x10) D4_1; else D4_0;
	delay_ms(1);
	E0;
}




void wh_write_cmd(uint8_t cmd, uint8_t wait)
{
//	wh_port->ODR = wh_port->IDR & (~wh_rw & ~wh_rs);
	RW0; RS0;
	wh_tetr_out(cmd);
	delay_ms(1);
	wh_tetr_out(cmd<<4);


	if (wait)
		delay_ms(10);
		//wh_wait_busy();
}

void wh_init(void)
{
	RCC_AHBPeriphClockCmd(   RCC_AHBPeriph_GPIOB , ENABLE);
	PIN_CONFIGURATION(RS_RESD);
	PIN_CONFIGURATION(EN_DC);
	PIN_CONFIGURATION(RW_CS);
	PIN_CONFIGURATION(DB7);
	PIN_CONFIGURATION(DB5_SCK);
	PIN_CONFIGURATION(DB6_SDO);
	PIN_CONFIGURATION(DB4_SDI);
/*	
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOB, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOC, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOD, ENABLE);
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOE, ENABLE);
 
    // Initialize all pins connected to the WH1602 module
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_RS_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_RS_PORT, &MT_GPIOcfg);
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_RW_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_RW_PORT, &MT_GPIOcfg);
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_E_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_E_PORT, &MT_GPIOcfg);
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_DB7_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_DB7_PORT, &MT_GPIOcfg);
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_DB6_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_DB6_PORT, &MT_GPIOcfg);
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_DB5_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_DB5_PORT, &MT_GPIOcfg);
 
    GPIO_StructInit(&MT_GPIOcfg);
    MT_GPIOcfg.GPIO_Pin = MT_WH1602_DB4_PIN;
    MT_GPIOcfg.GPIO_Mode = GPIO_Mode_OUT;
    MT_GPIOcfg.GPIO_Speed = GPIO_Speed_50MHz;
    GPIO_Init(MT_WH1602_DB4_PORT, &MT_GPIOcfg);	
*/	
	delay_ms(40); // 40 ms
//	wh_port->ODR = wh_port->IDR & (~wh_rw & ~wh_rs);
	RW0; RS0;
//	wh_tetr_out (0b00110000);
	wh_tetr_out (0x30);
	delay_ms(40); // 40 ms
//	wh_tetr_out (0b00110000);
	wh_tetr_out (0x30);
	delay_ms(40); // 40 ms
//	wh_tetr_out (0b00100000);
	wh_tetr_out (0x20);
	delay_ms(40); // 40 ms
//	wh_write_cmd(0b00101000, 0); // повторно
	wh_write_cmd(0x28, 0); // повторно
	delay_ms(40); // 40 ms
//	wh_write_cmd(0b00001100, 1); // 0b00001DBC, display on, both cursors off
	wh_write_cmd(0x0C, 1); // 0b00001DBC, display on, both cursors off
	delay_ms(1); // 40 us
//	wh_write_cmd(0b00000110, 1); //Assign cursor moving direction and enable the shift of entire display.
	wh_write_cmd(0x06, 1); //Assign cursor moving direction and enable the shift of entire display.
	delay_ms(1); // 40 us
//	wh_write_cmd(0b00000001, 1); // clear
	wh_write_cmd(0x01, 1); // clear
	delay_ms(1); // 40 us
//	wh_write_cmd(0b00000110, 1); //Assign cursor moving direction and enable the shift of entire display.
	wh_write_cmd(0x06, 1); //Assign cursor moving direction and enable the shift of entire display.
	delay_ms(1); // 40 us

//	wh_write_cmd(0b00000110, 1); // 0b0000 01(ID)(SH), increment on, display shift off
	wh_write_cmd(0x06, 1); // 0b0000 01(ID)(SH), increment on, display shift off
	delay_ms(1); // 40 us

//	wh_write_cmd(0b00000001, 1); // clear
	wh_write_cmd(0x01, 1); // clear
	delay_ms(1); // 40 us
}

void wh_set_cursor(uint8_t line, uint8_t pos)
{
	// pos will be used as a temporary variable
//	pos |= 0b10000000;
	pos |= 0x80;
//	if (line == 1)
//		pos += 0x40;

	switch(line)
	{
		case 0:
			break;
		case 1:
			pos += 0x40;
			break;
		case 2:
			pos += 0x14;
			break;
		case 3:
			pos += 0x54;
			break;
	}
	wh_write_cmd(pos, 1);
}

void lcd_clear(void)
{
	wh_write_cmd(1, 5);
}

void lcd_clear_line(uint8_t line)
{
	uint8_t i;
	wh_set_cursor( line, 0);
	for (i=0;i<16;i++) wh_write_chr(' ');
}
void wh_write_data(uint8_t data, uint8_t wait)
{
//	wh_port->ODR = wh_port->IDR & (~wh_rw);
//	wh_port->ODR = wh_port->IDR | wh_rs;
  	RW0; RS1;

	wh_tetr_out(data);
	delay_ms(1);
	wh_tetr_out(data<<4);


	if (wait)
		delay_ms(1);
		//wh_wait_busy();
}



void wh_write_str(char* string)
{
	uint8_t c;
	while ((c = (*string++) )!=0)
	{
		if(c >= 192)
			wh_write_data(russian[c-192], 1);
		else
			wh_write_data(c, 1);
	}
}

void wh_write_chr(char c)
{
		if((uint8_t)c >= 192)
			wh_write_data(russian[c-192], 1);
		else
			wh_write_data(c, 1);
}
