/******************** (C) COPYRIGHT 2011 STMicroelectronics ********************
* File Name          : hw_config.h
* Author             : MCD Application Team
* Version            : V3.3.0
* Date               : 21-March-2011
* Description        : Hardware Configuration & Setup
********************************************************************************
* THE PRESENT FIRMWARE WHICH IS FOR GUIDANCE ONLY AIMS AT PROVIDING CUSTOMERS
* WITH CODING INFORMATION REGARDING THEIR PRODUCTS IN ORDER FOR THEM TO SAVE TIME.
* AS A RESULT, STMICROELECTRONICS SHALL NOT BE HELD LIABLE FOR ANY DIRECT,
* INDIRECT OR CONSEQUENTIAL DAMAGES WITH RESPECT TO ANY CLAIMS ARISING FROM THE
* CONTENT OF SUCH FIRMWARE AND/OR THE USE MADE BY CUSTOMERS OF THE CODING
* INFORMATION CONTAINED HEREIN IN CONNECTION WITH THEIR PRODUCTS.
*******************************************************************************/

/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_CONFIG_H
#define __HW_CONFIG_H

/* Includes ------------------------------------------------------------------*/
#include "main.h"


/*
6. what command to send sms to use ( per exaple , if i count 10 pulses i want to send sms then ? )
*/

/* PIN DEFINES ------------------------------------------------------------*/


#define KEYMASK		(GPIO_Pin_6|GPIO_Pin_7|GPIO_Pin_8|GPIO_Pin_9)

/* MACROs for SET, RESET or TOGGLE Output port */
#define GPIO_HIGH(a,b) 		a->BSRRL = b
#define GPIO_LOW(a,b)			a->BSRRH = b
#define GPIO_TOGGLE(a,b) 	a->ODR ^= b 

//outputs
//porta
#define BLIGHT		A, 12, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define RS_RESD		B, 10, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define EN_DC			B, 11, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define RW_CS			B, 12, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define DB7				B, 2,  HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define DB5_SCK		B, 13, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define DB6_SDO		B, 14, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO
#define DB4_SDI		B, 15, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_400KHZ, AF_NO


#define LED1			B, 0, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED2			B, 8, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED3			C, 4, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED4			B, 5, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED5			A, 5, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED6			B, 3, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED7		  A, 0, HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO
#define LED8			C, 10,HIGH, MODE_OUTPUT_PUSH_PULL_PULL_DOWN, SPEED_10MHZ, AF_NO

//inputs
#define KEY1P			C, 6, HIGH, MODE_INPUT_PULL_UP, SPEED_2MHZ, AF_NO
#define KEY2P			C, 7, HIGH, MODE_INPUT_PULL_UP, SPEED_2MHZ, AF_NO
#define KEY3P			C, 8, HIGH, MODE_INPUT_PULL_UP, SPEED_2MHZ, AF_NO
#define KEY4P			C, 9, HIGH, MODE_INPUT_PULL_UP, SPEED_2MHZ, AF_NO

#define SW1				B, 1,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW2				B, 9,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW3				C, 5,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW4				B, 6,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW5				A, 6,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW6				B, 4,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW7				A, 1,  HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO
#define SW8				C, 11, HIGH, MODE_INPUT_PULL_UP, SPEED_10MHZ, AF_NO




/* Exported functions ------------------------------------------------------- */
void 	GPIO_LowPower_Config(void);
void 	Restore_GPIO_Config(void);

void 	Set_HWSystem(void);
void 	GPIO_Configuration(void);
void 	Config_Systick(void);
void 	USART_Configuration(void);
void 	EXTI_init(void);
//void 	ADC_Configuration(void);
//u16 	ReadADC12(u08 channel);



#endif  /*__HW_CONFIG_H*/
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
