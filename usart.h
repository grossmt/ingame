/* Define to prevent recursive inclusion -------------------------------------*/
#ifndef __HW_USART_H
#define __HW_USART_H

#include "main.h"
#include "type.h"


#define TBUF1_SIZE   128        /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/
#define RBUF1_SIZE   128        /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/

#define TBUF2_SIZE   4        /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/
#define RBUF2_SIZE   4        /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/

#define TBUF3_SIZE   256        /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/
#define RBUF3_SIZE   512        /*** Must be a power of 2 (2,4,8,16,32,64,128,256,512,...) ***/



#define TIMEOUTRX1	10	//in 10ms count
#define TIMEOUTRX2	10	//in 10ms count
#define TIMEOUTRX3	10	//in 10ms count



/* Includes ------------------------------------------------------------------*/
//for USART1
typedef struct
{
  unsigned int in;          /* Next In Index */
  unsigned int out;         /* Next Out Index */
  unsigned int count;
  unsigned int timeout;
  char buf [RBUF1_SIZE];    /* Buffer */
} rbuf1_st;
typedef struct  
{
  unsigned int in;          /* Next In Index */
  unsigned int out;         /* Next Out Index */
  unsigned int count;
  unsigned int txrestart;	
  char buf [TBUF1_SIZE];     /* Buffer */
} tbuf1_st;

//for USART2
typedef struct
{
  unsigned int in;          /* Next In Index */
  unsigned int out;         /* Next Out Index */
  unsigned int count;
  unsigned int timeout;
  char buf [RBUF2_SIZE];    /* Buffer */
} rbuf2_st;
typedef struct  
{
  unsigned int in;          /* Next In Index */
  unsigned int out;         /* Next Out Index */
  unsigned int count;
  unsigned int txrestart;	
  char buf [TBUF2_SIZE];     /* Buffer */
} tbuf2_st;

//for USART3
typedef struct
{
  unsigned int in;          /* Next In Index */
  unsigned int out;         /* Next Out Index */
  unsigned int count;
  unsigned int timeout;
  char buf [RBUF3_SIZE];    /* Buffer */
} rbuf3_st;
typedef struct  
{
  unsigned int in;          /* Next In Index */
  unsigned int out;         /* Next Out Index */
  unsigned int count;
  unsigned int txrestart;	
  char buf [TBUF3_SIZE];     /* Buffer */
} tbuf3_st;




/*-----------------------------------------------------------*/

void SetPort(int type);
int 			getkey (void);
int  			SendChar(int ch);
int  			GetKey(void);
void 			DecRxTimeout(void);
char 			putdataUART(int uart, char c);
int 			mgetchar0 (void) ;
//int 			mputchar (int PortNum, char sdata);
void 			mprintf(int PortNum, char *c);
void 			mprintfCR(int PortNum, char *c);
void 			UARTSend(int PortNum, u08 *c, u16 size);
int 			com_getchar (int PortNum) ;
char 			mgetchar(int PortNum) ;
int 			mscanf(int PortNum, char *ch, int max);
int 			getstring(int PortNum, char *ch);
unsigned int 	RxCount(int PortNum);
void 			DecRxTimeout(void);
unsigned int 	GetRxTimeout(int PortNum);
int 			uartNReceiveByte(int ComPort,char* rxData);
int 			copyRXbuf(int ComPort,char *outstring);
int 			WaitRxBlock(int PortNum,u32 WaitTime);
void 			flush_RxBuf(int PortNum);
u16				IsRxData(int PortNum);
int 			copyRXbuf_count(int ComPort,u08 *outstring,u16 count);
int 			checkTXsend(int PortNum);

void USART1_IRQHandler(void);
void USART2_IRQHandler(void);
void USART3_IRQHandler(void);

#endif 
