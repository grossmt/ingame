/* Includes ------------------------------------------------------------------*/

#include "usart.h"
//#include "timer.h"



#ifdef USART1_EN
volatile   rbuf1_st rbuf1 = { 0, 0, 0, 0,};
volatile   tbuf1_st tbuf1 = { 0, 0, 0, 1,};
#endif

#ifdef USART2_EN
volatile   rbuf2_st rbuf2 = { 0, 0, 0, 0,};
volatile   tbuf2_st tbuf2 = { 0, 0, 0, 1,};
#endif

#ifdef USART3_EN
volatile   rbuf3_st rbuf3 = { 0, 0, 0, 0,};
volatile   tbuf3_st tbuf3 = { 0, 0, 0, 1,};
#endif

volatile  int select_UART=1;

#define WAIT_KEY_TIME	10


void put_ByteUart1(uint8_t c);
int  get_ByteUart1(void);


void SetPort(int type)
{
	select_UART=type;
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
 int  GetKey(void)
{
     return com_getchar(select_UART);
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
int  SendChar(int c)
 	{
		//select_UART=DEB_UART;
		return (putdataUART(select_UART,c));                       
	}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
int checkTXsend(int PortNum)
{
    switch(PortNum) {
      case 1:

		  if  (tbuf1.in == tbuf1.out) return TRUE;	
          break;
         
      case 2:
#ifdef USART2_EN					
		  if  (tbuf2.in == tbuf2.out) return TRUE;	

          break;
#endif	
			
#ifdef USART3_EN				
      case 3:

		  if  (tbuf3.in == tbuf3.out) return TRUE;	

          break;
#endif 
      default:  
          break;
    }
	return FALSE;

}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
void mprintf(int PortNum, char *c)
{
char* ch;
ch=c;
    while(*ch) {
        putdataUART(PortNum, *ch++);
    }
		while (checkTXsend(PortNum)==FALSE);
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
void UARTSend(int PortNum, u08 *c, u16 size)
{
u08* ch;
ch=c;
		if (size==0) return;
    while(size--) 
		{
        putdataUART(PortNum, *(ch++));
    }
		while (checkTXsend(PortNum)==FALSE);
		
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
int copyRXbuf(int ComPort,char *outstring)
{
int temp;
int	i;

i=0;
while ((temp=com_getchar(ComPort))!=-1) 
	{
		*(outstring++)=(char)temp;
		i++;
	}
outstring[i]=0;	
return i;	
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
char mgetchar(int PortNum) 
{
  int ch;
  do 
  	{
    ch = com_getchar(PortNum);
  	}
  while (ch == -1);
  return ((char) ch);
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
int getstring(int PortNum, char *ch)
{
int m,i;
i=0;
	m=com_getchar(PortNum);		
	while ((m!=-1))
		{
		ch[i]=(char)m;
		i++;
		m=com_getchar(PortNum);
		}
	ch[i] = '\0';
  return i;
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
unsigned int GetRxTimeout(int PortNum)
{
	switch (PortNum)
		{
		case 1:
			{
			 return rbuf1.timeout;
			}
#ifdef	USART2_EN	
		case 2:
			{
			 return rbuf2.timeout;
			}
#endif			
			
#ifdef	USART3_EN		
		case 3:
			{
			 return rbuf3.timeout;
			}
#endif		
		}
   return 0;
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
void flush_RxBuf(int PortNum)
{
	switch (PortNum)
		{
		case 1:
			{
			rbuf1.in = rbuf1.out=rbuf1.count=0;
			tbuf1.in = tbuf1.out=0;
			}
		break;	
			
#ifdef	USART2_EN	
		case 2:
			{
			rbuf2.in = rbuf2.out=rbuf2.count=0;
			tbuf2.in = tbuf2.out=0;
			}
		break;	
#endif

#ifdef	USART3_EN				
		case 3:
			{
			rbuf3.in = rbuf3.out=rbuf3.count=0;
			tbuf3.in = tbuf3.out=0;
			}
		break;
#endif
			
		}
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
u16	IsRxData(int PortNum)
{
u16 count;
if (((count=RxCount(PortNum))!=0)&&((GetRxTimeout(PortNum)==0))) return count;
return 0; 
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
void DecRxTimeout(void)
{
	if (rbuf1.timeout) rbuf1.timeout--;
#ifdef	USART2_EN		
	if (rbuf2.timeout) rbuf2.timeout--;
#endif
	
#ifdef	USART3_EN		
	if (rbuf3.timeout) rbuf3.timeout--;
#endif
}

/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
unsigned int RxCount(int PortNum)
{
	switch (PortNum)
		{
		case 1:
			{
				return rbuf1.count;
			}
			
#ifdef	USART2_EN				
		case 2:
			{
				return rbuf2.count;
			}
#endif
			
#ifdef	USART3_EN				
		case 3:
			{
				return rbuf3.count;
			}
#endif
			
		}
   return 0;
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
int uartNReceiveByte(int ComPort,char* rxData)
{
	switch (ComPort)
		{
		case 1:
			{
				 if (rbuf1.count==0)	  
					{
						rbuf1.out=rbuf1.in=0;
						return FALSE;					
					}
					* rxData=rbuf1.buf [rbuf1.out++];
					rbuf1.out &=(RBUF1_SIZE - 1);
					USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
					--rbuf1.count;
					USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
					return TRUE;				
			}
	
#ifdef	USART2_EN				
		case 2:
			{
				 if (rbuf2.count==0)	  
					{
						rbuf2.out=rbuf2.in=0;
						return (FALSE);					
					}
					* rxData=rbuf2.buf [rbuf2.out++];
					rbuf2.out &=(RBUF2_SIZE - 1);
					USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
					--rbuf2.count;
					USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
					return TRUE;				
			}
#endif

#ifdef	USART3_EN				
		case 3:
			{
				 if (rbuf3.count==0)	  
					{
						rbuf3.out=rbuf3.in=0;
						return (FALSE);					
					}
					* rxData=rbuf3.buf [rbuf3.out++];
					rbuf3.out &=(RBUF3_SIZE - 1);
					USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
					--rbuf3.count;
					USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
					return TRUE;
			}
#endif
		}
return FALSE;
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
int com_getchar (int PortNum) 
{
uint8_t data;

	switch(PortNum) 
		{
       case 1:
				{
				 if (rbuf1.count==0)	  
					{
						rbuf1.out=rbuf1.in=0;
						return (-1);					
					}
					data=rbuf1.buf [rbuf1.out++];
					rbuf1.out &=(RBUF1_SIZE - 1);
					USART_ITConfig(USART1, USART_IT_RXNE, DISABLE);
					--rbuf1.count;
					USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
					return data;
				}
				
#ifdef	USART2_EN	
       case 2:
				{
				 if (rbuf2.count==0)	  
					{
						rbuf2.out=rbuf2.in=0;
						return (-1);					
					}
					data=rbuf2.buf [rbuf2.out++];
					rbuf2.out &=(RBUF2_SIZE - 1);
					USART_ITConfig(USART2, USART_IT_RXNE, DISABLE);
					--rbuf2.count;
					USART_ITConfig(USART2, USART_IT_RXNE, ENABLE);
					return data;
				}
#endif
				
#ifdef	USART3_EN	
        case 3:
				{
				 if (rbuf3.count==0)	  
					{
						rbuf3.out=rbuf3.in=0;
						return (-1);					
					}
					data=rbuf3.buf [rbuf3.out++];
					rbuf3.out &=(RBUF3_SIZE - 1);
					USART_ITConfig(USART3, USART_IT_RXNE, DISABLE);
					--rbuf3.count;
					USART_ITConfig(USART3, USART_IT_RXNE, ENABLE);
					return data;
				}
#endif				
	
//				case 7:
//				{
//					if (USB_Get_Data((uint8_t*)&data)) return data;
//					return -1;
//				}					
    	}
	return -1;	
}
/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
char putdataUART(int uart, char c)
{
	switch (uart)
	{
		case 1:
		{
			while (tbuf1.count == TBUF1_SIZE);
			USART_ITConfig(USART1, USART_IT_TXE, DISABLE);
			if (tbuf1.count || (USART_GetFlagStatus(USART1, USART_FLAG_TXE) == RESET))
				 {
				 tbuf1.buf [tbuf1.in++] = c;
				 tbuf1.in=tbuf1.in& (TBUF1_SIZE - 1);
				 ++tbuf1.count ;
				 USART_ITConfig(USART1, USART_IT_TXE, ENABLE);
				 }
				else
				USART_SendData(USART1,c);
				return (c);
		}
#ifdef	USART2_EN	
		case 2:
		{
			while (tbuf2.count == TBUF2_SIZE);
			USART_ITConfig(USART2, USART_IT_TXE, DISABLE);
			if (tbuf2.count || (USART_GetFlagStatus(USART2, USART_FLAG_TXE) == RESET))
				 {
				 tbuf2.buf [tbuf2.in++] = c;
				 tbuf2.in=tbuf2.in& (TBUF2_SIZE - 1);
				 ++tbuf2.count ;
				 USART_ITConfig(USART2, USART_IT_TXE, ENABLE);
				 }
				else
				USART_SendData(USART2,c);
				return (c);
		}
#endif

#ifdef	USART3_EN			
		case 3:
		{
			while (tbuf3.count == TBUF3_SIZE);
			USART_ITConfig(USART3, USART_IT_TXE, DISABLE);
			if (tbuf3.count || (USART_GetFlagStatus(USART3, USART_FLAG_TXE) == RESET))
				 {
				 tbuf3.buf [tbuf3.in++] = c;
				 tbuf3.in=tbuf3.in& (TBUF3_SIZE - 1);
				 ++tbuf3.count ;
				 USART_ITConfig(USART3, USART_IT_TXE, ENABLE);
				 }
				else
				USART_SendData(USART3,c);
				return (c);			
		}
#endif
				case 7:
		{
			wh_write_chr(c);
			return(c);
		}	
	}
	return(c);
}

/*------------------------------------------------------------------------------
------------------------------------------------------------------------------*/
void USART1_IRQHandler(void)
{ 
	
  if(USART_GetITStatus(USART1, USART_IT_RXNE) == SET)
  {
		if ((USART1->SR & (USART_FLAG_NE|USART_FLAG_FE|USART_FLAG_PE|USART_FLAG_ORE)) == 0)
		{                       
				rbuf1.buf[rbuf1.in++] = (char)(USART_ReceiveData(USART1)& 0xFF);
				rbuf1.timeout=TIMEOUTRX1;
				rbuf1.in &= (RBUF1_SIZE-1);
				++rbuf1.count;
				rbuf1.count &= (RBUF1_SIZE-1);
		}
	  else USART_ReceiveData(USART1);
  }
  if(USART_GetITStatus(USART1, USART_IT_ORE_RX) == SET) 
  {
     USART_ReceiveData(USART1); 
  }
  if(USART_GetITStatus(USART1, USART_IT_TXE) == SET)
  {   
			if (tbuf1.count)
			{
			 --tbuf1.count;
			  USART_SendData(USART1,tbuf1.buf [tbuf1.out++]);
			  tbuf1.out &= (TBUF1_SIZE-1);
			}
			else
			{
				USART_ITConfig(USART1, USART_IT_TXE, DISABLE);                  
			}
   }
}

#ifdef	USART2_EN	
void USART2_IRQHandler(void)
{ 
  if(USART_GetITStatus(USART2, USART_IT_RXNE) == SET)
  {
		if ((USART2->SR & (USART_FLAG_NE|USART_FLAG_FE|USART_FLAG_PE|USART_FLAG_ORE)) == 0)
		{  
				rbuf2.buf [rbuf2.in++ ]=(uint8_t)(USART_ReceiveData(USART2)& 0xFF);
				rbuf2.timeout=TIMEOUTRX2;
				rbuf2.in &= (RBUF2_SIZE-1);
				if (++rbuf2.count == RBUF2_SIZE) rbuf2.count=0;
		}
	  else USART_ReceiveData(USART2);
  }
  if(USART_GetITStatus(USART2, USART_IT_ORE_RX) == SET) 
  {
     USART_ReceiveData(USART2); 
  }
  if(USART_GetITStatus(USART2, USART_IT_TXE) == SET)
  {   
			if (tbuf2.count)
			{
			 --tbuf2.count;
			  USART_SendData(USART2,tbuf2.buf [tbuf2.out++]);
			  tbuf2.out &= (TBUF2_SIZE-1);
			}
			else
			{
				USART_ITConfig(USART2, USART_IT_TXE, DISABLE);                  
			}
   }
}

#endif

#ifdef	USART3_EN	
void USART3_IRQHandler(void)
{ 
  if(USART_GetITStatus(USART3, USART_IT_RXNE) == SET)
  {
		if ((USART3->SR & (USART_FLAG_NE|USART_FLAG_FE|USART_FLAG_PE|USART_FLAG_ORE)) == 0)
		{                       
				rbuf3.buf [rbuf3.in++ ]=(uint8_t)(USART_ReceiveData(USART3)& 0xFF);
				rbuf3.timeout=TIMEOUTRX3;
				rbuf3.in &= (RBUF3_SIZE-1);
				if (++rbuf3.count == RBUF3_SIZE) rbuf3.count=0;
		}
	  else USART_ReceiveData(USART3);
  }
  if(USART_GetITStatus(USART3, USART_IT_ORE_RX) == SET) 
  {
     USART_ReceiveData(USART3); 
  }
  if(USART_GetITStatus(USART3, USART_IT_TXE) == SET)
  {   
			if (tbuf3.count)
			{
			 --tbuf3.count;
			  USART_SendData(USART3,tbuf3.buf [tbuf3.out++]);
			  tbuf3.out &= (TBUF3_SIZE-1);
			}
			else
			{
				USART_ITConfig(USART3, USART_IT_TXE, DISABLE);                  
			}
   }
}




#endif
/******************************************************************************
**                            End Of File
******************************************************************************/
