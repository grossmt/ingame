#include "main.h"
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
volatile uint8_t	playMode=BR,changeMode=0;
Player player[8];

//char time[5];
volatile  u32 pause_time;
volatile  u32 current_time;
volatile  u32 prev_time;
volatile  char state=0;
volatile 	uint8_t volume = 1;
uint8_t queue[8];

volatile int who_pressed = -1;

const  Player InitPlayer=
{
	1, //enabled
	0, //keypressed
	0, //key state
	0, //timestamp
	0  //LED state
};

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
char ScanKey(void)
{
	
	if ((GPIO_ReadInputData(GPIOC)&KEYMASK)!=KEYMASK)
	{
		//BEEP(500,20,1);
		return 1;
	}
	return 0;
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
char keyPauseHandler(void)
{
	//checking for press PAUSE	
	if (PIN_SIGNAL(KEY2P)==0) //keyPAUSE was pressed
		{
			vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
			if (PIN_SIGNAL(KEY2P)==0) //debounce protect,PAUSE was pressed
			{
							
				#ifdef USE_WH1602	
					//wh_lcd_clear();
					//wh_write_str("�����");
				wh_write_str_center("   �����   ",LINE1);
				#endif
							
				state=PAUSED;//PAUSE STATE
				pause_time = current_time;
				BEEP(300, 10,volume);
				return 1;
			}
		}
		return 0;
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
char keyResetHandler(void)
{
				char i = 0;
		
				//waiting for press RESET	
				if (PIN_SIGNAL(KEY4P)==0) //keyRESET was pressed
					{
						vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
						if (PIN_SIGNAL(KEY4P)==0) //debounce protect,RESET was pressed
						{
										
							for (i=0;i<8;i++) 	
								player[i]=InitPlayer;
											
							who_pressed = -1;
								
							#ifdef USE_WH1602	
								wh_write_str_center("                ",LINE1);
							#endif	
							
							state=RESETED;

							BEEP(500,10,volume);
							while (PIN_SIGNAL(KEY4P)==0)
							{
								who_pressed = -1;
							}

							return 1;
						}
					}	
				return 0;
}
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
char keyStartHandler(char tSTATE, u32 tTIME)
{
	char i = 0;
					//waiting for press START	
					if (PIN_SIGNAL(KEY3P)==0) //keyStart was pressed
						{
							vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
							if (PIN_SIGNAL(KEY3P)==0) //debounce protect,START was pressed
							{
								for (i=0;i<8;i++) player[i].ledstate = NOTHING;															
								who_pressed = -1;
								state=tSTATE;
								BEEP(1000, 100,volume);
								prev_time = 0;
								SetGameTimer(tTIME);
								
//								#ifdef USE_WH1602	
//									wh_write_str_center("                ",LINE1);
//								#endif								
								
								return 1;
							}
						}	
						return 0;
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
int timeoverHandler(u32 timeover)
{
	if (current_time >= timeover*100)
				{
					state = FINISH;
					BEEP(300, 100,volume);
					
					#ifdef USE_WH1602	
						wh_write_str_center("����� ��������",LINE1);
					#endif
					return 1;
				}
	return 0;			
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void timeEndingHandler(u32 signalTime)
{
				signalTime *=100;
	
				if (current_time >= signalTime && current_time <= signalTime+10)
				{
					BEEP(500, 100,volume);
				}
}
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void printTime()
{
	char str[16];
				current_time = GetGameTimer();
				
				if (current_time - prev_time >=100 )
				{
					memset(str,0, sizeof(str));
					sprintf(str,"   �����: %d   ",current_time/100);
					#ifdef USE_WH1602	
						wh_write_str_center(str,LINE1);
					#endif
					prev_time = current_time;
				}	
}

/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void printPlayerTimestamp(uint8_t nplayer)
{
	char str[16];
	memset(str,0, sizeof(str));
	
	if (state == FALSTART)
	{
			sprintf(str, " P%d falstarted ", nplayer);
	}
	else 
	if (state == ANSWER)
	{
		sprintf(str, " P#%d TIME:%d.%02d ",nplayer,player[nplayer-1].keytimestamp/100,player[nplayer-1].keytimestamp%100);
	}
	
	#ifdef USE_WH1602	
		wh_write_str_center(str,LINE1);
	#endif
}


/*-----------------------------------------------------------*/

int keyResetVSHandler(int top,int bottom)
{
	int i = 0;
	
	if (PIN_SIGNAL(KEY4P)==0) //keyRESET was pressed
					{
						vTaskDelay(10);						//debounce protect, also we can increase this time for protecting from accident pressing
						if (PIN_SIGNAL(KEY4P)==0) //debounce protect,RESET was pressed
						{
	
							if (top == bottom + 1 || top == bottom)
							{
								//no players in queue to answer
								//clear all

								for (i=0; i<8; i++)
									queue[i] = 0;
								
								for (i=0;i<8;i++) 	
									player[i]=InitPlayer;
												
								who_pressed = -1;
									
								#ifdef USE_WH1602	
									wh_write_str_center("                ",LINE1);
								#endif	
								
								state=RESETED;

								BEEP(500,10,volume);
								while (PIN_SIGNAL(KEY4P)==0)
								{
									who_pressed = -1;
								}								
								
								return -2;
								
							}
							else
							{
								//remove top of queue
								//queue[bottom+1] = 0;
								
								player[queue[bottom+1]-1].ledstate = NOTHING;
								bottom ++;	
								
								while (PIN_SIGNAL(KEY4P)==0)
								{
									queue[bottom] = 0;
								}
								
									
							}
	
						}
					}
		return bottom;
}
/*-----------------------------------------------------------*/
void ShowMode(uint8_t mode)
{
#ifdef USE_WH1602		
	wh_set_cursor(0,0);
#endif
#ifdef USE_LCD320240			
 //vTaskDelay(1500);
#endif	
	switch (mode)
	{
		case CHGK:
		{
			wh_write_str_center("*��� ��� �����*",LINE0);
		}
		break;
		case BR:
		{
			wh_write_str_center("*����� ����*",LINE0);
		}
		break;
		case SI:
		{
			wh_write_str_center("*���� ����*",LINE0);
		}
		break;
		case VS:
		{
			wh_write_str_center("*� �������*",LINE0);
		}
		break;
	}
}/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
void vChangeModeTask(void *pvParameters)
{
	uint16_t counter = 0;//,prev_mode,timerconfirm;
	volatile uint8_t state_select=0;
	char str[16];

	
	while(1)
	{
		vTaskDelay(10);

		if (changeMode == RESETED)
		{
			//change game mode	
			if (PIN_SIGNAL(KEY1P)==0) //keyMODE was pressed
			{
				vTaskDelay(100);					//debounce protect, also we can increase this time for protecting from accident pressing
				if (PIN_SIGNAL(KEY1P)==0) //debounce protect
					{	
						BEEP(500,10,volume);
						state_select=1;
					
						if (counter >= 3) counter = 0; else	counter++; //switch to new mode
						ShowMode(counter); //show new mode on LCD
							
						while (PIN_SIGNAL(KEY1P)==0)
							state_select = 1;
										
					}
			}	

			playMode = counter;	
			
			//change volume
			if (PIN_SIGNAL(KEY2P)==0) //keyVOLUME was pressed
			{
				vTaskDelay(100);					//debounce protect, also we can increase this time for protecting from accident pressing
				if (PIN_SIGNAL(KEY2P)==0) //debounce protect
					{	

						#ifdef LEVELNEW
						while (PIN_SIGNAL(KEY2P)==0)
						{
							if (volume >= 15) 
							{
								wh_write_str_center("                ",LINE1);
								volume = 0; 
							}
							else	
							{
								volume++; //cicle of volume from 0 to 15
								lcdProgressBar(volume,LINE1);
							}
							vTaskDelay(80);	
						}
						BEEP(500,20,volume);
						vTaskDelay(500);
						#else	
							if (volume >= 2) volume = 0; else	volume++; //cicle of volume from 0 to 2
							memset(str,0, sizeof(str));
							sprintf(str, "   VOLUME: %d   ", volume);
							#ifdef USE_WH1602	
								wh_write_str_center(str,LINE1);
							#endif
						BEEP(500,50,volume);
						while (PIN_SIGNAL(KEY2P)==0)	volume = volume;
						vTaskDelay(1500);
						#endif						
						wh_write_str_center("                ",LINE1);
					}
			}	
		}				
	}			
}

/*-----------------------------------------------------------*/
void vGameTask(void *pvParameters)
{
	static volatile uint8_t prevchangeMode = 0;
	
	PIN_ON(BLIGHT);
	
	//vTaskDelay(300);
	//wh_set_cursor(0,0);
	wh_lcd_clear();
	wh_write_str_center("INTELLECTUAL",LINE0);
	wh_write_str_center("GAME",LINE1);
	volume = 0;
	BEEP(500,40,volume);
//#ifdef USE_WH1602		
	PIN_ON(BLIGHT);
	vTaskDelay(300);
	PIN_OFF(BLIGHT);
	vTaskDelay(300);
	PIN_ON(BLIGHT);
	vTaskDelay(300);
	PIN_OFF(BLIGHT);
	vTaskDelay(300);
	PIN_ON(BLIGHT);
	vTaskDelay(300);
	wh_lcd_clear();
//#endif
//#ifdef USE_LCD320240			
// vTaskDelay(1500);
// LCD_Fill(BLACK);
//#endif
	changeMode=0;
	prevchangeMode=99;	
	ShowMode(playMode);
	while(1)
	{
		switch (playMode)
		{
			case CHGK:
			{
				changeMode=FSMCHGK();
			}
			break;
			case BR:
			{
				changeMode=FSMBR();
			}
			break;
			case SI:
			{
				changeMode=FSMSI();
			}
			break;
			case VS:
			{
				changeMode=FSMVS();
			}
			break;
		}
		
	
	}
}
/*-----------------------------------------------------------*/
int main()
{
char i;
		//char str[32];
		Set_HWSystem();
		playMode=BR;
		volume=0;
		for (i=0;i<8;i++) 	player[i]=InitPlayer;
		printf("Game Pult Controller v1\n\r");
		//delay(50);
		wh_lcd_clear();
	  //delay(100);
//#ifdef USE_LCD320240	
//	//test_example();
//	LCD_Fill(BLACK);
//#endif		
	xTaskCreate(vGameTask,NULL, configMINIMAL_STACK_SIZE,	NULL, tskIDLE_PRIORITY + 1, NULL);
	xTaskCreate(vChangeModeTask,NULL, configMINIMAL_STACK_SIZE,	NULL, tskIDLE_PRIORITY + 2, NULL);

	vTaskStartScheduler();

}
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
// All Game FSM has to back value of current state: active or inactive
// if state is active - we can't change current mode
// Important!!!
// All Game FSM can't containes while(1) loops - all FSM must be out after one pass
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
char FSMCHGK(void)
{
/*
	wait press start - generate BEEP_Start
	wait 50 sec - generate BEEP_Remind10sec
	wait 60 sec - generate BEEP_StopDiscuss
	
	if admin will press RESET again during 1 min interval - game will be stopped
	
	if admin will press PAUSE during 1 min interval - game will be paused
	if admin will press START during PAUSED state - game will start again from the paused time
*/

	switch (state)
		{
			case RESETED:
			{
				keyStartHandler(COUNTDOWN,0);
			}
			break;
			
			case COUNTDOWN:
			{
				printTime();
				
				timeEndingHandler(50);
				
				timeoverHandler(60);
		
				keyPauseHandler();

				keyResetHandler();		
			
			}
			break;
			
			case FINISH:
			{
				keyResetHandler();
				keyStartHandler(COUNTDOWN,0);	
			}
			break;
			
			case PAUSED:
			{
					keyStartHandler(COUNTDOWN,prev_time);		
					keyResetHandler();
			}
		}
		return state;
}
/*-----------------------------------------------------------*/
char FSMBR(void)
{
	
	switch (state)
		{
			case RESETED:
			{
				if (who_pressed >=0)
				{
					state = FALSTART;	
				}
				else
				{
					keyStartHandler(COUNTDOWN,0);
					keyResetHandler();
				}
			}
			break;
			
			case COUNTDOWN:
			{
				printTime();
				timeEndingHandler(25);
				timeoverHandler(30);
				keyPauseHandler();
				keyResetHandler();

				if (who_pressed >=0)
				{
					state = ANSWER;					
				}					
			}
			break;
			
			case FINISH:
			{
				keyResetHandler();					
			}
			break;
			
			case PAUSED:
			{
					keyStartHandler(COUNTDOWN,prev_time);		
			}
			break;
			
			case FALSTART:
			{
				player[who_pressed].enabled = 0;
				player[who_pressed].keypressed = 1;
				player[who_pressed].ledstate = BLINKING3S;
				BEEP(300,100,volume);
				
				printPlayerTimestamp(who_pressed+1);
				
				vTaskDelay(1000);
				
				who_pressed = -1;
				
				//state = RESETED;
				
				while (!keyResetHandler())
				{
					who_pressed = -1;
				}
				
			}
			break;
			
			case ANSWER:
			{
				player[who_pressed].enabled = 0;
				player[who_pressed].keypressed = 1;
				player[who_pressed].ledstate = PERMANENT;
				BEEP(750,100,volume);
				
				printPlayerTimestamp(who_pressed+1);
				
				vTaskDelay(1000);  				
				
				who_pressed = -1;
				
				state = WAITTORECOUNTDOWN;
			}
			break;
			
			case WAITTORECOUNTDOWN:
			{
				keyStartHandler(RECOUNTDOWN,0);
				keyResetHandler();				
			}
			break;
			
			case RECOUNTDOWN:
			{
				printTime();
				timeEndingHandler(15);
				timeoverHandler(20);
				keyPauseHandler();
				keyResetHandler();

				if (who_pressed >=0)
				{
					state = ANSWER;					
				}					
			}
			break;
		}

	return state;
}
/*-----------------------------------------------------------*/
char FSMSI(void)
{
	switch (state)
		{
			case RESETED:
			{
				if (who_pressed >=0)
				{
					state = FALSTART;	
				}
				else
				{
					keyStartHandler(COUNTDOWN,0);
					keyResetHandler();
				}
			}
			break;
			
			case COUNTDOWN:
			{
				printTime();
				timeoverHandler(7);
				keyResetHandler();

				if (who_pressed >=0)
				{
					state = ANSWER;					
				}					
			}
			break;
			
			case FINISH:
			{
				keyResetHandler();					
			}
			break;
			
			case FALSTART:
			{
				player[who_pressed].ledstate = BLINKING3S;
				player[who_pressed].keypressed = 1;
				BEEP(300,100,volume);
				
				printPlayerTimestamp(who_pressed+1);
				
				vTaskDelay(1000);
				
				who_pressed = -1;
				
				//state = RESETED;
				while (!keyResetHandler())
				{
					who_pressed = -1;
				}
			}
			break;
			
			case ANSWER:
			{
				if (player[who_pressed].keypressed == 1 && player[who_pressed].keytimestamp < 200)
				{
					//player had falstarted and pressed again before 2sec has passed from start
					state = FALSTART;
				}
				else
				{
					//player, even if he had falstarted, is able to answer
					player[who_pressed].keypressed = 1;
					player[who_pressed].enabled = 0;
					player[who_pressed].ledstate = PERMANENT;
					BEEP(750,100,volume);
					
					printPlayerTimestamp(who_pressed+1);
					
					who_pressed = -1;
					
					state = WAITTORECOUNTDOWN;
						
				}

			}
			break;
			
			case WAITTORECOUNTDOWN:
			{
				keyResetHandler();
			}
			
			break;
		}	
	
	return state;
}
/*-----------------------------------------------------------*/
char FSMVS(void)
{
	char str[16];
	static uint8_t shift = 0;
	int i;
	static int top = -1, bottom = -1, del = -1;
	
	switch (state)
	{
		case RESETED:
		{
			if (who_pressed >=0)
			{
				state = ANSWER;
			}
			
			del = keyResetVSHandler(top,bottom);
			
			if (del == -2)
			{
				top = -1;
				bottom = -1;
			}
			else
			if (bottom != del)
			{
				bottom = del;
				state = PRINTANSWER;
			}
			
			//bottom = keyResetVSHandler(top,bottom);
		}
		break;
		
		case ANSWER:
		{
			queue[++top] = who_pressed+1;
			player[who_pressed].keypressed = 1;
			player[who_pressed].enabled = 0;
			BEEP(750,50,volume);
			
			who_pressed = -1;
			
			state = PRINTANSWER;
		}
		break;
		
		case PRINTANSWER:
		{
				
				player[queue[bottom+1]-1].ledstate = PERMANENT;
			
				for (i=bottom+2;i<=top;i++)
					player[queue[i]-1].ledstate = BLINKING;
			
				//fill the printline by the queue of players;
				memset(str,0, sizeof(str));
				shift = 0;
				for (i = bottom+1; i <=top; i++)
				{
					sprintf(str + shift ,"P%d",queue[i]);
					shift+=2;
				}
				
				#ifdef USE_WH1602	
					wh_write_str_center("                ",LINE1);				
					wh_write_str_center(str,LINE1);
				#endif				
				
				state = RESETED;
		}
		break;
		
	}	
	
	return state;
}
/*-----------------------------------------------------------*/
//warp over LCD print
#define MAX_LENGHT_WH1602		16
#define MAX_LENGHT_TFT			19
void wh_write_str_center(char* string,uint8_t line)
{
	
	uint8_t size;//,x;
	char* ch=string;

//#ifdef USE_WH1602	
	wh_write_str("                ");
	size=0;
	while (*ch++) size++;
	size--;
	if (size>MAX_LENGHT_WH1602) size=MAX_LENGHT_WH1602;
	wh_set_cursor(line,MAX_LENGHT_WH1602/2-size/2-1);
	wh_write_str(string);
//#endif
//#ifdef USE_LCD320240	
//	size=0;
//	while (*ch++) size++;
//	size--;
//	if (size>MAX_LENGHT_TFT) size=MAX_LENGHT_TFT;
//	x=(MAX_LENGHT_TFT/2+1-size/2)*15;
//	WriteString(line,0,"                                                    ",YELLOW);//clear line SPACE char has narrow widht
//	WriteString(line,x,(unsigned char *)string,YELLOW);
//#endif	
}
/*-----------------------------------------------------------*/
/*-----------------------------------------------------------*/
//void vFreeRTOSInitAll()
//{
//		char i;
//		//char str[32];
//	
//		Set_HWSystem();
//	
//		playMode=CHGK;
//	
//		for (i=0;i<8;i++) 
//			player[i]=InitPlayer;
//		
//		printf("Game Pult Controller v1\n\r");
//		delay(50);
//		wh_lcd_clear();
//		//wh_set_cursor(0,0);
////		sprintf(str,"TIME %d",i++);
////		wh_write_str((char*)str);
////						//wh_write_str(time);
//////PIN_OFF(BLIGHT);
//////PIN_OFF(EN_DC);
//////PIN_OFF(RW_CS);
//////PIN_OFF(RS_RESD);
//////PIN_OFF(DB7);
//////PIN_OFF(DB6_SDO);
//////PIN_OFF(DB5_SCK);
//////PIN_OFF(DB4_SDI);	
////delayms(1000);
////	  while (1)
////		{
////			wh_write_chr('U');
////			//sprintf(str,"TIME %d",i++);
////			//wh_write_str_center(str,LINE0);
////			//wh_write_str(time);
////PIN_ON(BLIGHT);
//////PIN_ON(EN_DC);
//////PIN_ON(RW_CS);
//////PIN_ON(RS_RESD);
//////PIN_ON(DB7);
//////PIN_ON(DB6_SDO);
//////PIN_ON(DB5_SCK);
//////PIN_ON(DB4_SDI);			
////			
////			delayms(100);
////PIN_OFF(BLIGHT);
//////PIN_OFF(EN_DC);
//////PIN_OFF(RW_CS);
//////PIN_OFF(RS_RESD);
//////PIN_OFF(DB7);
//////PIN_OFF(DB6_SDO);
//////PIN_OFF(DB5_SCK);
//////PIN_OFF(DB4_SDI);
////			
////			delayms(100);
////		}

//}
