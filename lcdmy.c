
#include "lcdmy.h"

#define delay_LCD  delayms
#define MUL_DEL		1	

#define WH_4BIT


#define E1   	PIN_ON(EN_DC)
#define E0   	PIN_OFF(EN_DC)
//#define RW1  	PIN_ON(RW_CS) //never use - always write into LCD
#define RW0  	PIN_OFF(RW_CS)
#define RS1  	PIN_ON(RS_RESD)
#define RS0  	PIN_OFF(RS_RESD)

#define D7_0	PIN_OFF(DB7)
#define D7_1	PIN_ON(DB7)
#define D6_0	PIN_OFF(DB6_SDO)
#define D6_1	PIN_ON(DB6_SDO)
#define D5_0	PIN_OFF(DB5_SCK)
#define D5_1	PIN_ON(DB5_SCK)
#define D4_0	PIN_OFF(DB4_SDI)
#define D4_1	PIN_ON(DB4_SDI)

//#define BLIGHT		A, 12, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define RS_RESD		B, 10, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define EN_DC			B, 11, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define RW_CS			B, 12, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define DB7				B, 2,  HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define DB5_SCK		B, 13, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define DB6_SDO		B, 14, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO
//#define DB4_SDI		B, 15, HIGH, MODE_OUTPUT_PUSH_PULL, SPEED_2MHZ, AF_NO

//#define E1   	GPIO_SetBits	(GPIOB, GPIO_Pin_11);	
//#define E0    GPIO_ResetBits(GPIOB, GPIO_Pin_11);
//#define RW1   GPIO_SetBits	(GPIOB, GPIO_Pin_12);	
//#define RW0  	GPIO_ResetBits(GPIOB, GPIO_Pin_12);
//#define RS1   GPIO_SetBits	(GPIOB, GPIO_Pin_10);	
//#define RS0   GPIO_ResetBits(GPIOB, GPIO_Pin_10);

//#define D7_0	 GPIO_ResetBits(GPIOB, GPIO_Pin_2);
//#define D7_1	 GPIO_SetBits	 (GPIOB, GPIO_Pin_2);	
//#define D6_0	 GPIO_ResetBits(GPIOB, GPIO_Pin_14);
//#define D6_1	 GPIO_SetBits	 (GPIOB, GPIO_Pin_14);	
//#define D5_0	 GPIO_ResetBits(GPIOB, GPIO_Pin_13);
//#define D5_1	 GPIO_SetBits	 (GPIOB, GPIO_Pin_13);	
//#define D4_0	 GPIO_ResetBits(GPIOB, GPIO_Pin_15);
//#define D4_1	 GPIO_SetBits	 (GPIOB, GPIO_Pin_15);	


static const uint8_t russian[]={ 0x41, 0xA0, 0x42, 0xA1, 0xE0, 0x45,
0xA3, 0xA4, 0xA5,0xA6, 0x4B, 0xA7, 0x4D, 0x48, 0x4F, 0xA8, 0x50,0x43,
0x54, 0xA9, 0xAA, 0x58, 0xE1, 0xAB, 0xAC, 0xE2, 0xAD,0xAE, 0x62,
0xAF, 0xB0, 0xB1, 0x61, 0xB2, 0xB3, 0xB4, 0xE3, 0x65, 0xB6, 0xB7,
0xB8, 0xB9, 0xBA, 0xBB, 0xBC, 0xBD, 0x6F, 0xBE, 0x70, 0x63,0xBF,
0x79, 0xE4, 0x78, 0xE5, 0xC0, 0xC1, 0xE6, 0xC2, 0xC3, 0xC4, 0xC5,
0xC6, 0xC7 };


const char customchar0[8] = {
0x1F, // # # # # #
0x1F, // # # # # #
0x1F, // # # # # #
0x1F, // # # # # #
0x1F, // # # # # #
0x1F, // # # # # #
0x1F, // # # # # #
0x1F  // # # # # # full dot
};
//**************************************************************************************************
void wh_tetr_out(uint8_t tetr)
{
	E0;	
	if(tetr & 0x80) D7_1 else D7_0;
	if(tetr & 0x40) D6_1 else D6_0;
	if(tetr & 0x20) D5_1 else D5_0;
	if(tetr & 0x10) D4_1 else D4_0;
	E1;
	delay_LCD(1);
	E0;
	D7_0;D6_0;D5_0;D4_0;
}
//**************************************************************************************************
void wh_write_cmd(uint8_t cmd)
{
	RW0; RS0;
	delay_LCD(1*MUL_DEL);
	wh_tetr_out(cmd);
	delay_LCD(1*MUL_DEL);
	wh_tetr_out(cmd<<4);
	delay_LCD(1*MUL_DEL);
//	if (wait)	
//		delay_LCD(wait*MUL_DEL);
	//RW1;
}
//**************************************************************************************************
void wh_write_data(uint8_t data)
{
 	RW0; RS1;
	delay_LCD(1*MUL_DEL);
	wh_tetr_out(data);
	delay_LCD(1*MUL_DEL);
	wh_tetr_out(data<<4);
	delay_LCD(1*MUL_DEL);
//	if (wait)
//		delay_LCD(wait*MUL_DEL);
	//RW1;
}
//**************************************************************************************************
void wh_lcd_init(void) 
{
	GPIO_InitTypeDef GPIO_InitStructure;
  
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_GPIOB , ENABLE);
	
  GPIO_InitStructure.GPIO_Pin = GPIO_Pin_2|GPIO_Pin_10|GPIO_Pin_11|GPIO_Pin_12|GPIO_Pin_13|GPIO_Pin_14|GPIO_Pin_15;
  GPIO_InitStructure.GPIO_Mode = GPIO_Mode_OUT;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
  GPIO_InitStructure.GPIO_PuPd = GPIO_PuPd_DOWN;
  GPIO_InitStructure.GPIO_Speed = GPIO_Speed_400KHz;
  GPIO_Init(GPIOB, &GPIO_InitStructure);
	
	PIN_CONFIGURATION(RS_RESD);
	PIN_CONFIGURATION(EN_DC);
	PIN_CONFIGURATION(RW_CS);
	PIN_CONFIGURATION(DB7);
	PIN_CONFIGURATION(DB5_SCK);
	PIN_CONFIGURATION(DB6_SDO);
	PIN_CONFIGURATION(DB4_SDI);

	 
	
	delay_LCD(100);
	RW0; RS0;E0; 
	delay_LCD(100);
//	wh_tetr_out(0x33);
//	wh_tetr_out(0x33);
//	wh_tetr_out(0x33);
//	wh_tetr_out(0x22);
//	wh_write_cmd(0x28);
//	wh_write_cmd(0x01);
//	wh_write_cmd(0x10);
//	wh_write_cmd(0x06);
//	wh_write_cmd(0x0C);

	wh_tetr_out (0x30);
	delay_LCD(40*MUL_DEL); // 40 ms
	wh_tetr_out (0x30);
	delay_LCD(40*MUL_DEL); // 40 ms
	wh_tetr_out (0x30);
	delay_LCD(40*MUL_DEL); // 40 ms	
	wh_tetr_out (0x20);
	delay_LCD(40*MUL_DEL); // 40 ms
	
	wh_write_cmd(0x28); 	// повторно
	delay_LCD(40*MUL_DEL); // 40 ms

	wh_write_cmd(0x0C); // 0b00001DBC, display on, both cursors off
	delay_LCD(1*MUL_DEL); // 40 us

	wh_write_cmd(0x06); //Assign cursor moving direction and enable the shift of entire display.
	delay_LCD(1*MUL_DEL); // 40 us

	wh_write_cmd(0x01); // clear
	delay_LCD(1*MUL_DEL); // 40 us

	wh_write_cmd(0x06); //Assign cursor moving direction and enable the shift of entire display.
	delay_LCD(1*MUL_DEL); // 40 us

	wh_write_cmd(0x06); // 0b0000 01(ID)(SH), increment on, display shift off
	delay_LCD(1*MUL_DEL); // 40 us

	wh_write_cmd(0x01); // clear
	delay_LCD(1*MUL_DEL); // 40 us
	
	DefineChar(1,(char*)customchar0);

}
//**************************************************************************************************
void wh_set_cursor(uint8_t line, uint8_t pos)
{
	pos |= 0x80;
	switch(line)
	{
		case 0:
			break;
		case 1:
			pos += 0x40;
			break;
		case 2:
			pos += 0x14;
			break;
		case 3:
			pos += 0x54;
			break;
	}
	wh_write_cmd(pos);
}
//**************************************************************************************************
void wh_lcd_clear(void)
{
	wh_write_cmd(1);
	delay_LCD(10);
	wh_set_cursor(0,0);
	//wh_write_cmd(2, 5);	
}
//**************************************************************************************************
void wh_cursorEnable(uint8_t on) 
{
	if (on == 1)
	{
		//lcd_write_cmd(0x0e);
		wh_write_cmd(0x0e);
	}
	else
	{
		//lcd_write_cmd(0x0c);
		wh_write_cmd(0x0c);
	}
 // delay_LCD(1);
}
//**************************************************************************************************
void wh_write_str(char* string)
{
	uint8_t c;
	while ((c = (*string++) )!=0)
	{
		wh_write_chr(c);
//		if(c >= 192)		wh_write_data(russian[c-192],1);
//		else	wh_write_data(c,1);
	}
}
void wh_write_str_center_1602(char* string,uint8_t line)
{
	uint8_t size;
	char* ch=string;
	wh_write_str("                ");
	size=0;
	while (*ch++) size++;
	size--;
	if (size>16) size=16;
	wh_set_cursor(line,7-size/2);
	wh_write_str(string);
//	while ((c = (*string++) )!=0)
//	{
//		if(c >= 192)	wh_write_data(russian[c-192],1);
//		else	wh_write_data(c,1);
//	}
}
//**************************************************************************************************
void wh_write_chr(char c)
{
		if((uint8_t)c >= 192)	wh_write_data(russian[c-192]);
		else	wh_write_data(c);
}	
//**************************************************************************************************
void lcdProgressBar(uint8_t progress, uint8_t line)
{
	uint8_t i;
	if (progress>15) progress=15;
	wh_set_cursor(line,0);
	for (i=0;i<progress;i++)
	{
		wh_write_chr(1);
	}
}
//**************************************************************************************************
//input charnum 0-7, array with customchar pixels
int DefineChar(int charnum, char* values)
{
int i;
if ((charnum < 0) || (charnum > 7)) return(-1);
// set LCD address to CG RAM
wh_write_cmd((0x40 + charnum*8));
// enter character bytes into CG RAM
for (i=0; i<8; i++) {
wh_write_data(values[i]);
}
return(0);
}
//**************************************************************************************************
/*
example of using
lcd_init(16);
DefineChar(customchar0,1); 
wh_write_chr(1);//or sprintf(str, "customchar0 is \1");
*/


